# Service Manager Description #
The Service Manager is responsible of managing the Security Mechanisms (SMs) and Security Capabilities (SCs) by using a persistent data storage that is CRUD enabled.
The main design requirement imposed the possibility to query the Security Mechanisms and Security Capabilities database in order to obtain Mechanisms that are able to enforce or monitor specific metrics and satisfy specific capabilities. Moreover, the component should allow the End User (EU) to get the associated metadata of each Security Mechanism.

### Use Cases ###

![Service Manager 2 - Use Case.jpg](https://bitbucket.org/repo/6XrjAA/images/2268453506-Service%20Manager%202%20-%20Use%20Case.jpg)

## Installation ##

**Install using precompiled binaries**

The precompiled binaries are available under the SPECS Artifact Repository (http://ftp.specs-project.eu/public/artifacts/)

Prerequisites:

* Oracle Java JDK 7;
* SQLite 3.9.x;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);

Installation steps:

* download the web application archive (war) file from the artifact repository :
http://ftp.specs-project.eu/public/artifacts/sla-platform/service-manager/service-manager-STABLE.war
* the war file has to be deployed in the java servlet/web container

If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.

**Compile and install from source**

In order to compile and install the Services Manager it is mandatory first to process the backend and afterwards the frontend.

Prerequisites:

* a Git client;
* Apache Maven 3.3.x;
* Oracle Java JDK 7;
* SQLite 3.9.x;
* Java Servlet/Web Container (recommended: Apache Tomcat 7.0.x);

Backend installation steps:

* clone the Bitbucket repository:
```
#!bash
git clone git@bitbucket.org:specs-team/specs-core-sla_platform-service_manager.git
```
* change the configuration of the database in the persistence.xml file (the file is located in the folder “src/main/resource” and “/src/test/resources”) in order to define the correct path where the SQLite database will be created;
* under specs-core-sla_platform-service_manager run:

```
#!bash
mvn install
```

Frontend installation steps:

* clone the Bitbucket repository:
```
#!bash
git clone git@bitbucket.org:specs-team/specs-core-sla_platform-service_manager-api.git
```
* under specs-core-sla_platform-service_manager-api run:

```
#!bash
mvn package
```

The backend installation generates the artifact used by the frontend. The frontend installation generates a web application archive (war) file, under the “/target” subfolder. In order to use the component, the war file has to be deployed in the java servlet/web container. If Apache Tomcat 7.0.x is used, the war file needs to be copied into the “/webapps” folder inside the home directory (CATALINA_HOME) of Apache Tomcat 7.0.x.


## Usage ##
The Service Manager component exposes a REST API interface. All the exposed REST resources are mapped under the path “/cloud-sla/*”. The mapping rules are defined in the web.xml file under WEB-INF folder (CATALINA_HOME/webapps/service-manager-api/WEB-INF/):


```
#!xml
<servlet-mapping>
		<servlet-name>Jersey REST Service</servlet-name>
		<url-pattern>/cloud-sla/*</url-pattern>
</servlet-mapping>
```

Moreover, all the REST resources are represented by specific java classes under the package “eu.specsproject.slaplatform.servicemanager.restfrontend”, defined in the web.xml file under WEB-INF folder:

```
#!xml
<init-param>
	<param-name>jersey.config.server.provider.packages</param-name>
	<param-value>eu.specsproject.slaplatform.servicemanager.restfrontend</param-value>
</init-param>
```

At the startup of the component, all the REST resources are configured based on the configuration parameters defined in the web.xml file.


##Rest API Calls - Example##

![API 1.png](https://bitbucket.org/repo/6XrjAA/images/3440584209-API%201.png)

![API 2.png](https://bitbucket.org/repo/6XrjAA/images/2134505751-API%202.png)

![API 3.png](https://bitbucket.org/repo/6XrjAA/images/1874722649-API%203.png)

### Who do I talk to? ###

* Please contact massimiliano.rak@unina2.it
* www.specs-project.eu

SPECS Project 2013 - CeRICT