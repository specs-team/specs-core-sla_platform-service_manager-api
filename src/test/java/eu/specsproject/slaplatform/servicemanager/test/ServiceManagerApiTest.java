package eu.specsproject.slaplatform.servicemanager.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.test.framework.AppDescriptor;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import eu.specsproject.slaplatform.servicemanager.ServiceManager;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismDocument;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityCapabilityIdentifier;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismIdentifier;
import eu.specsproject.slaplatform.servicemanager.internal.marshalling.MarshallingInterface;
import eu.specsproject.slaplatform.servicemanager.api.restbackend.ServicesAPI;
import eu.specsproject.slaplatform.servicemanager.api.restfrontend.SCAnnotationsResource;
import eu.specsproject.slaplatform.servicemanager.api.restfrontend.SCResource;
import eu.specsproject.slaplatform.servicemanager.api.restfrontend.SMAnnotationsResource;
import eu.specsproject.slaplatform.servicemanager.api.restfrontend.SMMetadataResource;
import eu.specsproject.slaplatform.servicemanager.api.restfrontend.SMResource;
import eu.specsproject.slaplatform.servicemanager.api.restfrontend.utils.SerializationProvider;
import eu.specsproject.slaplatform.servicemanager.api.restfrontend.utils.TestPersistence;

public class ServiceManagerApiTest extends JerseyTest{

	private static ServiceManager sm;
	private static SecurityMechanismIdentifier id;
	private static SecurityCapabilityIdentifier idc;
	private static SecurityMechanismDocument smd;
	private static SerializationProvider sp;
	private SMResource smr;

	private static String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
		}
	}

	@Override
	protected AppDescriptor configure() {
		return new WebAppDescriptor.Builder().build();
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.out.println("setUpBeforeClass Called");
		TestPersistence.setEnableTest(true);
		JerseyEmbeddedHTTPServerCrunchify.startServer();
		ServicesAPI sapi = ServicesAPI.getInstance();
		Assert.assertNotNull(sapi);

		sm = sapi.getManager();
		Assert.assertNotNull(sm);

		smd = new SecurityMechanismDocument(readFile("src/test/resources/mechanism-webpool.json"));
		id = sm.createSM(smd);

		List<String> capabilities = (sm.retrieveSM(id)).getSecurityCapabilities();
		idc = sm.createSC(capabilities.get(0));

		sp = new SerializationProvider();
		Assert.assertNotNull(sp);
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("tearDown Called");
	}

	@Test
	public final void smResourceTest() throws UniformInterfaceException, IOException {
		smr = new SMResource(id.toString());
		Assert.assertNotNull(smr);

		String smString = smr.getSM();
		Assert.assertNotNull(smString);

		String updateSm = "update-SM";
		smr.updateSM(updateSm);
		Assert.assertNotEquals(updateSm, smr.getSM());

		String before = smr.getSM();
		updateSm = null;    	
		smr.updateSM(updateSm);
		Assert.assertEquals(before, smr.getSM());

		SMMetadataResource smMdR = smr.getMetadata();
		Assert.assertNotNull(smMdR);
		Assert.assertEquals(smMdR.getMetadata(), sm.retrieveSMMetadata(id));

		SMAnnotationsResource smAr = smr.getAnnotationResource();
		Assert.assertNotNull(smAr);

		Response r = smr.removeSM();
		Assert.assertEquals(id.toString(), r.getEntity());
	}

	@Test
	public final void smsResourceTest() throws UniformInterfaceException, IOException{

		ClientConfig cc = new DefaultClientConfig();
		Client client = Client.create(cc);

		WebResource wr = client.resource("http://localhost:8089/security-mechanisms");
		ClientResponse response = wr.type(MediaType.TEXT_PLAIN).post(ClientResponse.class, readFile("src/test/resources/mechanism-sva.json"));
		String post = response.getEntity(String.class);
		System.out.println(post);
		Assert.assertNotNull(post);

		wr = client.resource("http://localhost:8089/security-mechanisms/"+post);
		response = wr.get(ClientResponse.class);
		String responseGet = response.getEntity(String.class);
		System.out.println(responseGet);
		Assert.assertNotNull(responseGet);
		//     	Assert.assertEquals(200,response.getStatus());
		//     	Assert.assertEquals("documento",responseGet);
	}

	@Test
	public final void smsResourceGetTest(){
		ClientConfig cc = new DefaultClientConfig();
		Client client = Client.create(cc);

		WebResource wr = client.resource("http://localhost:8089/security-mechanisms");
		ClientResponse response = wr.get(ClientResponse.class);
		String responseString = response.getEntity(String.class);
		Assert.assertNotNull(responseString);

		wr = client.resource("http://localhost:8089/security-mechanisms?capability_id=test");
		response = wr.get(ClientResponse.class);
		responseString = response.getEntity(String.class);
		Assert.assertNotNull(responseString);

		wr = client.resource("http://localhost:8089/security-mechanisms?metric_id=test");
		response = wr.get(ClientResponse.class);
		responseString = response.getEntity(String.class);
		Assert.assertNotNull(responseString);

		wr = client.resource("http://localhost:8089/security-mechanisms?items=1");
		response = wr.get(ClientResponse.class);
		responseString = response.getEntity(String.class);
		Assert.assertNotNull(responseString);

		wr = client.resource("http://localhost:8089/security-mechanisms?page=1&lenght=1");
		response = wr.get(ClientResponse.class);
		responseString = response.getEntity(String.class);
		Assert.assertNotNull(responseString);

		wr = client.resource("http://localhost:8089/SM1");
		response = wr.get(ClientResponse.class);
		responseString = response.getEntity(String.class);
		Assert.assertNotNull(responseString);
		//    	Assert.assertEquals(200, response.getStatus());
	}

	@Test
	public final void scResourceTest() throws UniformInterfaceException, IOException {

		SCResource scr = new SCResource(idc.toString());
		Assert.assertNotNull(scr);

		String secCap = scr.getSC();
		Assert.assertNotNull(secCap);

		String beforeMod = scr.getSC();
		secCap = "updated-capability";
		scr.updateSC(secCap);
		Assert.assertNotEquals(beforeMod, scr.getSC());

		SCAnnotationsResource scAr = scr.getAnnotationResource();
		Assert.assertNotNull(scAr);

		Response r = scr.removeSC();
		Assert.assertEquals(idc.toString(), r.getEntity());
	}

	@Test
	public final void scsResourceTest() throws UniformInterfaceException, IOException{
		ClientConfig cc = new DefaultClientConfig();
		Client client = Client.create(cc);

		WebResource wr = client.resource("http://localhost:8089/security-capabilities");
		ClientResponse response = wr.type(MediaType.TEXT_PLAIN).post(ClientResponse.class, "capability");
		String post = response.getEntity(String.class);
		System.out.println(post);
		Assert.assertNotNull(post);

		wr = client.resource("http://localhost:8089/security-capabilities/"+post);
		response = wr.get(ClientResponse.class);
		String responseGet = response.getEntity(String.class);
		System.out.println(responseGet);
		Assert.assertNotNull(responseGet);
	}

	@Test
	public final void scsResourceGetTest(){
		int totIt = 0;
		int page = 0;
		int length = 0;    	

		ClientConfig cc = new DefaultClientConfig();
		Client client = Client.create(cc);

		WebResource wr = client.resource("http://localhost:8089/security-capabilities"
				+"?items="+totIt
				+"&page="+page
				+"&length="+length);
		ClientResponse response = wr.get(ClientResponse.class);
		String post = response.getEntity(String.class);
		Assert.assertNotNull(post);

		wr = client.resource("http://localhost:8089/SM1");
		response = wr.get(ClientResponse.class);
		post = response.getEntity(String.class);
		Assert.assertNotNull(post);
	}

	@Test
	public final void scAnnotationResourceTest(){
		SCAnnotationsResource scar = new SCAnnotationsResource(idc);
		Annotation a = scar.getAnnotations();
		Assert.assertNull(a);

		scar.updateAnnotation("my-new-sc-annotation");
		Assert.assertNull(scar.getAnnotations());
	}

	@Test
	public final void smAnnotationResourceTest(){
		SMAnnotationsResource smar = new SMAnnotationsResource(id);
		Annotation a = smar.getAnnotations();
		Assert.assertNull(a);

		smar.updateAnnotation("my-new-sm-annotation");
		Assert.assertNull(smar.getAnnotations());
	}

	@Test
	public final void serializationProviderTest() throws IOException{
		String xml = "<string name=string_name>text_string</string>";
		InputStream is = new ByteArrayInputStream(xml.getBytes());

		Assert.assertTrue(sp.isReadable(MarshallingInterface.class, null,null,null));

		Assert.assertNull(sp.readFrom(eu.specsproject.slaplatform.servicemanager.internal.marshalling.MarshallingInterface.class, null, null, MediaType.APPLICATION_XML_TYPE,null, is));

		Assert.assertNotNull(sp.readFrom(eu.specsproject.slaplatform.servicemanager.internal.marshalling.MarshallingInterface.class, null, null, MediaType.APPLICATION_JSON_TYPE,null, is));

		long s = sp.getSize(new MarshallingInterface(){}, MarshallingInterface.class, null, null, null);
		Assert.assertEquals(0, s);
	}
}
