package eu.specsproject.slaplatform.servicemanager.api.restfrontend.utils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import eu.specsproject.slaplatform.servicemanager.PersistenceEntityManager;

public class PersistenceAppListener implements ServletContextListener {

	public void contextInitialized(ServletContextEvent evt) {
	}

	public void contextDestroyed(ServletContextEvent evt) {
		PersistenceEntityManager.getInstance().closeEntityManagerFactory();
	}
}