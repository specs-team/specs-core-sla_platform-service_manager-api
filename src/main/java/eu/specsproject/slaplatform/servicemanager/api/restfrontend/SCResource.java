/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.api.restfrontend;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specsproject.slaplatform.servicemanager.api.restbackend.ServicesAPI;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityCapabilityIdentifier;
import eu.specsproject.slaplatform.servicemanager.internal.EUServiceManagerAbstractImpl;



public class SCResource {

    private SecurityCapabilityIdentifier id ;

    public SCResource (String id){
        this.id=new SecurityCapabilityIdentifier(Integer.valueOf(id));
    }
 
    @GET
    public String getSC(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(ServicesAPI.getInstance().getManager().retrieveSC(id));
        } catch (JsonProcessingException e) {
            LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("JsonProcessingException",e);
            return e.getMessage();
        }
    }
    
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public void updateSC(String newSC){
        
        if (newSC!=null){
            ServicesAPI.getInstance().getManager().updateSC(id, newSC); 
        }
    }
    
    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    public Response removeSC(){
        ServicesAPI.getInstance().getManager().removeSC(id);
        return Response.ok(id.toString()).build();
    }

    
    @Path("/annotations")
    public SCAnnotationsResource getAnnotationResource(){
        return new SCAnnotationsResource(id);
    }
}
