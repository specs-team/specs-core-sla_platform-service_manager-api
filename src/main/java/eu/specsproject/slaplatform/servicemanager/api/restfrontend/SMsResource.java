/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.api.restfrontend;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specsproject.slaplatform.servicemanager.api.restbackend.ServicesAPI;
import eu.specsproject.slaplatform.servicemanager.entities.CollectionSchema;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismDocument;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityMechanismIdentifier;
import eu.specsproject.slaplatform.servicemanager.internal.EUServiceManagerAbstractImpl;

@Path("/security-mechanisms")
public class SMsResource {

    @Context
    UriInfo uriInfo; 

    @GET
    @Produces({MediaType.APPLICATION_JSON })
    public Response getSMS(@QueryParam("capability_id") String capabilityId, @QueryParam("capabilities_id") String capabilitiesId, 
            @QueryParam("metric_id") String metricId, @QueryParam("metrics_id") String metricsId, 
            @QueryParam("items") Integer totalItems, 
            @QueryParam("page") Integer page, @QueryParam("length") Integer length) {

        if((capabilityId != null ^ capabilitiesId != null ^ metricId != null ^ metricsId != null) || (capabilityId == null && capabilitiesId == null && metricId == null && metricsId == null)){
            if(page == null && length == null && totalItems == null){
                totalItems = 50;
            }
            
            int start = (page != null && length != null && totalItems == null) ? page : 0; 
            int stop = (totalItems != null) ? totalItems : -1; 
            stop = (page != null && length != null && totalItems == null) ? page+length : stop;
            
            ObjectMapper mapper = new ObjectMapper();
            List<SecurityMechanismIdentifier> sms;
            try {
                List<String> capabilityList = null;
                List<String> metricsList = null;
                if(capabilitiesId != null){
                    capabilityList = mapper.readValue(capabilitiesId, List.class);
                }else if (capabilityId != null){
                    capabilityList = new ArrayList<String>();
                    capabilityList.add(capabilityId);
                }
                if(metricsId != null){
                    metricsList = mapper.readValue(metricsId, List.class);
                }else if (metricId != null){
                    metricsList = new ArrayList<String>();
                    metricsList.add(metricId);
                }
                sms = ServicesAPI.getInstance().getManager().searchSMs(capabilityList, metricsList, start, stop);
            } catch (IOException e) {
                LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("IOException",e);
                return Response.serverError().entity(new String(e.getMessage())).build();
            }

            List <String> identifiers = addIndentifiers(sms);

            CollectionSchema schema = new CollectionSchema("Security Mechanisms",sms.size(),stop-start,identifiers);
            try {
                return Response.ok(mapper.writeValueAsString(schema)).build();
            } catch (JsonProcessingException e) {
                LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("JsonProcessingException",e);
                return Response.serverError().entity(new String(e.getMessage())).build();
            }
            
        }else{
            return Response.status(400).entity(new String("Only the capabilities search OR metrics search is allowed!")).build();
        }

        
    }

    private List <String> addIndentifiers(List <SecurityMechanismIdentifier> sms){
        List <String> identifiers = new ArrayList<String>();
        for (int j = 0; j < sms.size(); j++){
            String basePath = "/".equals(uriInfo.getAbsolutePath().toString().substring(uriInfo.getAbsolutePath().toString().length()-1)) ? 
                    uriInfo.getAbsolutePath().toString().substring(0, uriInfo.getAbsolutePath().toString().length()-1) : uriInfo.getAbsolutePath().toString();
                    identifiers.add(basePath+"/"+sms.get(j).getId());
        }
        return identifiers;
    }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createSM(String document){

        try {
            SecurityMechanismIdentifier id = ServicesAPI.getInstance().getManager().createSM(new SecurityMechanismDocument(document));
            UriBuilder ub = uriInfo.getAbsolutePathBuilder();
            URI smURI = ub.
                    path(id.toString()).
                    build();
            return Response.created(smURI).entity(id.toString()).build();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        return Response.serverError().build();

    }

    @Path("/{id}")
    public SMResource getSMresource(@PathParam("id") String id) {
        return new SMResource(id);
    }

}
