/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.api.restfrontend;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.specsproject.slaplatform.servicemanager.api.restbackend.ServicesAPI;
import eu.specsproject.slaplatform.servicemanager.entities.CollectionSchema;
import eu.specsproject.slaplatform.servicemanager.entities.SecurityCapabilityIdentifier;
import eu.specsproject.slaplatform.servicemanager.internal.EUServiceManagerAbstractImpl;

@Path("/security-capabilities")
public class SCsResource {

    @Context
    UriInfo uriInfo; 
    
    @GET
    @Produces({MediaType.APPLICATION_JSON })
    public String getSCS(@QueryParam("items") Integer totalItems, 
            @QueryParam("page") Integer page, @QueryParam("length") Integer length) {
        
        List <SecurityCapabilityIdentifier> scs = ServicesAPI.getInstance().getManager().searchSCs();
        
        if(page == null && length == null && totalItems == null){
            totalItems = 50;
        }
        
        List <String> identifiers = new ArrayList<String>();
        int start = (page != null && length != null && totalItems == null) ? page : 0; 
        int stop = (totalItems != null) ? totalItems : scs.size(); 
        stop = (page != null && length != null && totalItems == null) ? page+length : stop; 
        stop = (stop > scs.size()) ? scs.size() : stop;
        
        for (int j = start; j < stop; j++){
            identifiers.add(uriInfo.getAbsolutePath()+"/"+scs.get(j).getId());
        }

        CollectionSchema schema = new CollectionSchema("Security Capabilities",scs.size(),stop-start,identifiers);
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(schema);
        } catch (JsonProcessingException e) {
            LoggerFactory.getLogger(EUServiceManagerAbstractImpl.class).debug("JsonProcessingException",e);
            return e.getMessage();
        }
    }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createSC(String document){
        SecurityCapabilityIdentifier id = ServicesAPI.getInstance().getManager().createSC(document); 
        UriBuilder ub = uriInfo.getAbsolutePathBuilder();
        URI smURI = ub.
                path(id.toString()).
                build();
        return Response.created(smURI).entity(id.toString()).build();
    }

    @Path("/{id}")
    public SCResource getSCresource(@PathParam("id") String id) {
        return new SCResource(id);
    }

}
