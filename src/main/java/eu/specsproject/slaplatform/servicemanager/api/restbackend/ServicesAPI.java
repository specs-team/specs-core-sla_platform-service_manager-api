/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.servicemanager.api.restbackend;

import eu.specsproject.slaplatform.servicemanager.ServiceManager;
import eu.specsproject.slaplatform.servicemanager.ServiceManagerFactory;
import eu.specsproject.slaplatform.servicemanager.api.restfrontend.utils.TestPersistence;


public class ServicesAPI {
    
    private static ServicesAPI instance = null;
    private ServiceManager managerEU;
    private static boolean isTest = false;
    
    private ServicesAPI(){
        managerEU = ServiceManagerFactory.getServiceManagerInstance(isTest);
    }
    
    
    public static ServicesAPI getInstance() {
        if (instance==null){
            if(!TestPersistence.isEnableTest()){
                instance = new ServicesAPI();
            }else{
            	isTest = true;
                instance = new ServicesAPI();
            }
        }
        
        return instance;
    }
    
    public ServiceManager getManager(){
        return managerEU;
    }
    

}
